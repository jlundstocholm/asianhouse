﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

using AsianHouse.DataLogic;
using AsianHouse.Common.Objects;

namespace AsianHouse.Controls
{
    public class ItemGroupControl : System.Web.UI.Control
    {
        public override void RenderControl(System.Web.UI.HtmlTextWriter writer)
        {
            int count = 1;

            List<AsianHouse.Common.Objects.ItemGroup> itemGroups = Data.GetItemGroups();

            writer.Write(@"<table width=""100%"">");
            foreach (AsianHouse.Common.Objects.ItemGroup itemGroup in itemGroups)
            {
                writer.Write(GetTableCell(count, itemGroup));
                count++;
            }


            writer.Write(@"</table>");


            base.RenderControl(writer);
        }

        private string GetTableCell(int index_, AsianHouse.Common.Objects.ItemGroup itemGroup_)
        {
            if (index_ % 2 == 1)
            {
                return String.Format(@"<tr><td colspan=""4"" style=""font-size: 8pt;background: #000000;"">&nbsp;</td></tr><tr><td style=""text-align:left;"">{0}</td><td>{1}</td>", GetImageRef(itemGroup_), GetTitleRef(itemGroup_).Trim());
            }
            else
            {
                return String.Format(@"<td style=""text-align: right;"">{0}</td><td style=""text-align:right;vertical-align:center;"">{1}</td></tr><tr><td colspan=""4"">&nbsp;</td></tr>", GetTitleRef(itemGroup_).Trim(), GetImageRef(itemGroup_));
            }
        }

        private string GetImageRef(AsianHouse.Common.Objects.ItemGroup itemGroup_)
        {
            return String.Format(@"<a href=""varegruppe.aspx?id={0}""><img alt=""{1}"" src=""/view.aspx?picture=varegrupper/{2}""/></a>", itemGroup_.Id, itemGroup_.GroupName, itemGroup_.ImageFile);
        }

        private string GetTitleRef(AsianHouse.Common.Objects.ItemGroup itemGroup_)
        {
            //return itemGroup_.GroupName;
            return String.Format(@"<a href=""varegruppe.aspx?id={0}"">{1}</a>", itemGroup_.Id, itemGroup_.GroupName);
        }
    }
}
