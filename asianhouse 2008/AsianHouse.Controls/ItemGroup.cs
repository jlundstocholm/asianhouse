﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

using AsianHouse.Common.Enums;
using AsianHouse.DataLogic;
using AsianHouse.Common.Interfaces;

namespace AsianHouse.Controls
{
    public class ItemGroup : System.Web.UI.WebControls.WebControl
    {
        int m_itemGroupID = 0;

        public int ItemGroupID
        {
            set { m_itemGroupID = value; }
        }

        Alignment m_Alignment;
        public Alignment Alignment
        {
            set
            {
                m_Alignment = value;
            }
        }

        public override void RenderControl(System.Web.UI.HtmlTextWriter writer)
        {
            IItemGroup itemGroup = Data.GetItemGroup(m_itemGroupID);

            string output = string.Format(@"<div style=""width:325px;float:{0};text-align:center;margin-top:10pt;"">
        <a href=""varegruppe.aspx?id={1}"">
                    <img style=""border: 1px solid #f00;"" alt=""{2}"" src=""/view.aspx?picture=varegrupper/{3}"" /></a>
                    <br />
                    <a href=""varegruppe.aspx?id={1}"">{2}</a>
        </div>", m_Alignment == Alignment.Left ? "left" : "right", itemGroup.Id, itemGroup.GroupName, itemGroup.ImageFile);

            writer.Write(output);
            base.RenderControl(writer);
        }
    }
}
