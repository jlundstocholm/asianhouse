﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;

using AsianHouse.Common.Enums;
using AsianHouse.DataLogic;
using AsianHouse.Common.Interfaces;

namespace AsianHouse.Controls
{
    public class ItemGroupContent : System.Web.UI.WebControls.WebControl
    {
        int m_itemGroupID = 0;

        public int ItemGroupID
        {
            set { m_itemGroupID = value; }
        }

        public override void RenderControl(System.Web.UI.HtmlTextWriter writer)
        {
            List<IItem> items = Data.GetItemsForGroup(m_itemGroupID);

            CultureInfo cInfo = new CultureInfo("da-DK");
            cInfo.NumberFormat.NumberDecimalDigits = 2;
            cInfo.NumberFormat.NumberDecimalSeparator = ",";

            Thread.CurrentThread.CurrentCulture = cInfo;

            writer.Write("<table>");



            foreach (IItem item in items)
            {
                StringBuilder sb = new StringBuilder();
                
                sb.AppendFormat(@"<tr style=""background-color: #000000;"">
			<td></td>
			<td>Produkt</td>
			<td>Varenummer</td>
			<td>Længde</td>
			<td>Højde</td>
			<td>Bredde</td>
			<td>Vægt</td>
		</tr>
		<tr style=""vertical-align:top;"">
			<td rowspan=""2"">
				<a href=""item.aspx?itemid={0}&size=med""><img style=""border: 1px solid #f00;"" src=""/view.aspx?item={0}"" alt=""Billede af vare""/></a>
			</td>
			<td style=""vertical-align:top;""><h2>{1}</h2></td>
			<td class=""center"">{2}</td>
			<td >{3}</td>
			<td >{4}</td>
			<td >{5}</td>
			<td >{6}</td>
		</tr>
		<tr style=""vertical-align:top;"">
			<td colspan=""7"" style=""text-align:left;"">{7}{8}</td>
		</tr>", item.Guid, 
              item.Name, 
              item.SKU, 
              item.Length, 
              item.Height, 
              item.Width, 
              item.Weight, 
              item.Description.Replace(Environment.NewLine, "<br/>"),
              Context.User.Identity.IsAuthenticated ? string.Format("<br/><br/>Pris: {0} ex moms", item.Price == null ? "" : ((double)item.Price).ToString("0.00")) : "");

                writer.Write(sb.ToString());
            }

            writer.Write("</table>");


            base.RenderControl(writer);
        }
    }
}
