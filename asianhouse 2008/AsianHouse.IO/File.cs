﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

using AsianHouse.Common;
using AsianHouse.Common.Interfaces;
using AsianHouse.Common.Enums;
using AsianHouse.DataLogic;
using AsianHouse.Drawing;

namespace AsianHouse.IO
{
    public class File
    {
        // Fields
        private string m_displayMode;
        private string m_downloadMode;
        private string m_fileName;

        // Methods
        public File(string fileName_, string downloadMode_)
        {
            if ((fileName_ == null) || (fileName_.ToString().Length == 0))
            {
                throw new Exception("File name passed cannot be empty nor null");
            }
            this.m_fileName = fileName_.ToString();
            this.m_downloadMode = downloadMode_;
            if (!System.IO.File.Exists(this.FilePath))
            {
                throw new Exception("File could not be found");
            }
        }

        public File(string Guid_, string downloadMode_, string displayMode_, bool IsFile_)
        {
            this.m_downloadMode = downloadMode_;
            this.m_displayMode = displayMode_;
            if ((Guid_ == null) || (Guid_.ToString().Length == 0))
            {
                throw new IOException("Identifier passed cannot be empty nor null");
            }

            IItem item = Data.GetItem(new Guid(Guid_));
            

            switch (this.DisplayMode)
            {
                case DisplayMode.Full:
                    this.m_fileName = item.ImageFile + ".jpg";
                    break;

                case DisplayMode.Medium:
                    this.m_fileName = item.ImageFile + ".med.jpg";
                    break;

                case DisplayMode.Thumbnail:
                    this.m_fileName = item.ImageFile + ".thumb.jpg";
                    break;
            }
        }

        public static void CreatePictures(Stream fileStream_, string fileName_, Guid itemGuid_, string folderName_)
        {
            try
            {
                AsianHouse.Drawing.Image AHimage = new AsianHouse.Drawing.Image();

                System.Drawing.Image originalImage = AHimage.GetOriginalImage(fileStream_);

                int scaledWidth = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["ImageWidth"]);
                int scaledWidthThumb = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["ImageWidthThumbnail"]);

                AsianHouse.Drawing.ImageSize size = new ImageSize(originalImage.Height, originalImage.Width, scaledWidth, scaledWidthThumb);

                //Trace.Write(scaledWidth.ToString());
                //Trace.Write(scaledWidthThumb.ToString());

                double sizeRatio = AHimage.Ratio(originalImage.Width, scaledWidth);
                //Trace.Write(sizeRatio.ToString());
                int newHeight = Convert.ToInt32(originalImage.Height * sizeRatio);
                int newWidth = Convert.ToInt32(originalImage.Width * sizeRatio);


                string extension = "jpg";
                string fileName = fileName_;

                string newFile = String.Format("{0}.{1}", fileName, extension);

                string pathOriginal = System.IO.Path.Combine(folderName_, String.Format("{0}-{2}.{1}", fileName, extension, itemGuid_));
                string pathMedium = System.IO.Path.Combine(folderName_, String.Format("{0}-{3}{1}.{2}", fileName, ".med", extension, itemGuid_));
                string pathThumb = System.IO.Path.Combine(folderName_, String.Format("{0}-{3}{1}.{2}", fileName, ".thumb", extension, itemGuid_));

                // save original picture
                originalImage.Save(pathOriginal, ImageFormat.Jpeg);

                // save medium picture
                System.Drawing.Image newImage = AHimage.ScalePicture(originalImage, size.ImageWidth, size.ImageHeight);
                newImage.Save(pathMedium, ImageFormat.Jpeg);

                // save thumbnail
                newImage = AHimage.ScalePicture(originalImage, size.ThumbWidth, size.ThumbHeight);
                newImage.Save(pathThumb, ImageFormat.Jpeg);
            }
            catch (Exception ex_)
            {
                throw ex_;
            }

        }


        public static void DeletePictureFiles(string imageFile_)
        {
            try
            {
                if (System.IO.File.Exists(imageFile_ + ".jpg"))
                {
                    System.IO.File.Delete(imageFile_ + ".jpg");
                }
                if (System.IO.File.Exists(imageFile_ + ".med.jpg"))
                {
                    System.IO.File.Delete(imageFile_ + ".med.jpg");
                }
                if (System.IO.File.Exists(imageFile_ + ".thumb.jpg"))
                {
                    System.IO.File.Delete(imageFile_ + ".thumb.jpg");
                }
            }
            catch
            {
            }
        }

        // Properties
        public string ContentType
        {
            get
            {
                switch (this.Extension)
                {
                    case "jpg":
                        return "image/jpeg";

                    case "jpeg":
                        return "image/jpeg";

                    case "doc":
                        return "application/ms-word";
                }
                return "application/octet-stream";
            }
        }

        public DisplayMode DisplayMode
        {
            get
            {
                switch (this.m_displayMode)
                {
                    case "med":
                        return DisplayMode.Medium;

                    case "full":
                        return DisplayMode.Full;

                    case null:
                        return DisplayMode.Thumbnail;
                }
                return DisplayMode.Thumbnail;
            }
        }

        public DownloadMode DownloadMode
        {
            get
            {
                string str;
                if ((this.m_downloadMode != null) && (((str = this.m_downloadMode) != null) && (string.IsInterned(str) == "download")))
                {
                    return DownloadMode.Download;
                }
                return DownloadMode.Display;
            }
        }

        public string Extension
        {
            get
            {
                string[] strArray = this.m_fileName.Split(new char[] { '.' });
                return strArray[strArray.Length - 1].ToLower();
            }
        }

        public string FileName
        {
            get
            {
                return this.m_fileName;
            }
        }

        public string FilePath
        {
            get
            {
                string str = ConfigurationSettings.AppSettings["FilePath"];
                return (str + this.m_fileName);
            }
        }
    }


}
