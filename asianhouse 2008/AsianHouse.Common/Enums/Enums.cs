﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AsianHouse.Common.Enums
{
    public enum Alignment
    {
        Left,
        Right
    }

    public enum DisplayMode
    {
        Medium,
        Full,
        Thumbnail
    }

    public enum DownloadMode
    {
        Display,
        Download
    }

    public enum Role
    {
        Administrator,
        Retailer,
        NotDefined
    }

}
