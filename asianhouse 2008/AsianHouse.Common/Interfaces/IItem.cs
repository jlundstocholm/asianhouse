﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AsianHouse.Common.Interfaces
{
    public interface IItem
    {
        string SKU { get; set; }
        string Name { get; set; }
        double? Height { get; set; }
        double? Width { get; set; }
        double? Price { get; set; }
        string CurrencyCode { get; set; }
        double? Weight { get; set; }
        double? Length { get; set; }
        string Description { get; set; }
        System.Guid Guid { get; set; }
        int ItemGroupID { get; set; }
        DateTime CreationDate { get; set; }
        bool IsActive { get; set; }
        int SortOrder { get; set; }
        string ImageFile { get; set; }
    }
}
