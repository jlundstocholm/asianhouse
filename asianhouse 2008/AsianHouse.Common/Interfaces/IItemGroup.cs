﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AsianHouse.Common.Interfaces
{
    public interface IItemGroup
    {
        string GroupName { get; set; }
        int Id { get; set; }
        string ImageFile { get; set; }
        int Priority { get; set; }
    }
}
