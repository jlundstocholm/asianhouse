﻿using System.Configuration;

namespace AsianHouse.Common
{
    public static class SecurityManager
    {
        public static string HashPassword(string password)
        {
            var hashIV = ConfigurationManager.AppSettings["PasswordIV"];

            var prependedPswd = hashIV + password;

            return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(prependedPswd, "sha1");
        }
    }
}
