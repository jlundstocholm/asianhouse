﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AsianHouse.Common.Objects
{
    public class Exhibition
    {
        string m_Title;

        public string Title
        {
            get { return m_Title; }
            set { m_Title = value; }
        }
        string m_SubTitle;

        public string SubTitle
        {
            get { return m_SubTitle; }
            set { m_SubTitle = value; }
        }
        string m_Location;

        public string Location
        {
            get { return m_Location; }
            set { m_Location = value; }
        }
        DateTime m_StartDate;

        public DateTime StartDate
        {
            get { return m_StartDate; }
            set { m_StartDate = value; }
        }
        DateTime m_EndDate;

        public DateTime EndDate
        {
            get { return m_EndDate; }
            set { m_EndDate = value; }
        }
        string m_Hall;

        public string Hall
        {
            get { return m_Hall; }
            set { m_Hall = value; }
        }
        string m_Spot;

        public string Spot
        {
            get { return m_Spot; }
            set { m_Spot = value; }
        }
        bool m_IsActive;

        public bool IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }
        }
        string m_ImageFile;

        public string ImageFile
        {
            get { return m_ImageFile; }
            set { m_ImageFile = value; }
        }
    }
}
