﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AsianHouse.Common.Interfaces;

namespace AsianHouse.Common.Objects
{
    public class Item : IItem
    {
        #region IItem Members

        string m_SKU = "";
        public string SKU
        {
            get
            {
                return m_SKU;
            }
            set
            {
                m_SKU = value;
            }
        }

        string m_Name = "";
        public string Name
        {
            get
            {
                return m_Name;
            }
            set
            {
                m_Name = value;
            }
        }

        string m_ImageFile = "";
        public string ImageFile
        {
            get
            {
                return m_ImageFile;
            }
            set
            {
                m_ImageFile = value;
            }
        }

        double? m_Height;
        public double? Height
        {
            get
            {
                return m_Height;
            }
            set
            {
                m_Height = value;
            }
        }

        double? m_Width;
        public double? Width
        {
            get
            {
                return m_Width;
            }
            set
            {
                m_Width = value;
            }
        }

        double? m_Weight;
        public double? Weight
        {
            get
            {
                return m_Weight;
            }
            set
            {
                m_Weight = value;
            }
        }

        string m_Description = "";
        public string Description
        {
            get
            {
                return m_Description;
            }
            set
            {
                m_Description = value;
            }
        }

        System.Guid m_Guid;
        public Guid Guid
        {
            get
            {
                return m_Guid;
            }
            set
            {
                m_Guid = value;
            }
        }

        int m_ItemGroupID;
        public int ItemGroupID
        {
            get
            {
                return m_ItemGroupID;
            }
            set
            {
                m_ItemGroupID = value;
            }
        }

        DateTime m_CreationDate;
        public DateTime CreationDate
        {
            get
            {
                return m_CreationDate;
            }
            set
            {
                m_CreationDate = value;
            }
        }

        bool m_IsActive;
        public bool IsActive
        {
            get
            {
                return m_IsActive;
            }
            set
            {
                m_IsActive = value;
            }
        }

        int m_SortOrder;
        public int SortOrder
        {
            get
            {
                return m_SortOrder;
            }
            set
            {
                m_SortOrder = value;
            }
        }

        string m_CurrencyCode = "";
        public string CurrencyCode
        {
            get
            {
                return m_CurrencyCode;
            }
            set
            {
                m_CurrencyCode = value;
            }
        }

        double? m_Price;
        public double? Price
        {
            get
            {
                return m_Price;
            }
            set
            {
                m_Price = value;
            }
        }

        double? m_Length;
        public double? Length
        {
            get
            {
                return m_Length;
            }
            set
            {
                m_Length = value;
            }
        }

        #endregion
    }
}
