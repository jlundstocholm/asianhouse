﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AsianHouse.Common.Interfaces;

namespace AsianHouse.Common.Objects
{
    public class ItemGroup : IItemGroup, IComparable<ItemGroup>
    {

        #region IComparable<ItemGroup> Members

        public int CompareTo(ItemGroup other)
        {
            return this.GroupName.CompareTo(other.GroupName);
        }

        #endregion


        #region IItemGroup Members

        string m_GroupName;
        public string GroupName
        {
            get
            {
                return m_GroupName;
            }
            set
            {
                m_GroupName = value;
            }
        }

        int m_Id = 0;
        public int Id
        {
            get
            {
                return m_Id;
            }
            set
            {
                m_Id = value;
            }
        }

        string m_ImageFile;
        public string ImageFile
        {
            get
            {
                return m_ImageFile;
            }
            set
            {
                m_ImageFile = value;
            }
        }

        int m_Priority = 0;
        public int Priority
        {
            get
            {
                return m_Priority;
            }
            set
            {
                m_Priority = value;
            }
        }

        #endregion

    }
}
