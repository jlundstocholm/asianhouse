﻿using System.Security.Principal;

namespace AsianHouse.Common.Objects
{
    public class UserIdentity : IIdentity
    {
        public string Email { get; set; }
        public string Roles { get; private set; }


        public UserIdentity(string name, string roles)
        {
            Name = name;
            Roles = roles;
            IsAuthenticated = true;
        }

        public UserIdentity() {}

        public UserIdentity(string name, string emailAddress, string roles)
        {
            Name = name;
            Email = emailAddress;
            Roles = roles;
            IsAuthenticated = true;
        }

        #region IIdentity Members

        public string AuthenticationType
        {
            get { return "MySql"; }
        }

        public bool IsAuthenticated { get; private set; }

        public string Name { get; set; }

        #endregion
    }
}
