﻿using System;
using System.Security.Principal;

namespace AsianHouse.Common
{
    public class UserPrincipal : IPrincipal
    {
        private readonly IIdentity _identity;
        private readonly string[] _roles;


        public UserPrincipal(IIdentity identity, string roles)
        {
            _identity = identity;
            _roles = roles.Split('|');
            Array.Sort(_roles);
        }


        #region IPrincipal Members

        // IPrincipal Implementation
        public bool IsInRole(string role)
        {
            return Array.BinarySearch(_roles, role) >= 0 ? true : false;
        }
        public IIdentity Identity
        {
            get
            {
                return _identity;
            }
        }

        #endregion
    }
}
