﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace AsianHouse.Drawing
{
    public class Image
    {
        // Methods
        public static ImageCodecInfo GetImageCodec(string encoding_)
        {
            foreach (ImageCodecInfo info in ImageCodecInfo.GetImageDecoders())
            {
                //Trace.Write(info.MimeType);
                if (encoding_ == info.MimeType)
                {
                    return info;
                }
            }
            return null;
        }

        public System.Drawing.Image GetOriginalImage(Stream imageStream_)
        {
            return System.Drawing.Image.FromStream(imageStream_);
        }

        public double Ratio(int originalSize_, int desiredSize_)
        {
            return (((double)desiredSize_) / ((double)originalSize_));
        }

        public System.Drawing.Image ScalePicture(System.Drawing.Image imgPhoto, int Width, int Height)
        {
            int width = imgPhoto.Width;
            int height = imgPhoto.Height;
            int x = 0;
            int y = 0;
            int num5 = 0;
            int num6 = 0;
            float num7 = 0f;
            float num8 = 0f;
            float num9 = 0f;
            num8 = ((float)Width) / ((float)width);
            num9 = ((float)Height) / ((float)height);
            if (num9 < num8)
            {
                num7 = num9;
                num5 = (int)((Width - (width * num7)) / 2f);
            }
            else
            {
                num7 = num8;
                num6 = (int)((Height - (height * num7)) / 2f);
            }
            int num10 = (int)(width * num7);
            int num11 = (int)(height * num7);
            Bitmap image = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
            image.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);
            Graphics graphics = Graphics.FromImage(image);
            graphics.Clear(Color.Red);
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.DrawImage(imgPhoto, new Rectangle(num5, num6, num10, num11), new Rectangle(x, y, width, height), GraphicsUnit.Pixel);
            graphics.Dispose();
            return image;
        }
    }

    public struct ImageSize
    {
        public int ImageHeight;
        public int ImageWidth;
        public int ThumbWidth;
        public int ThumbHeight;
        public ImageSize(int originalImageHeight_, int originalImageWidth_, int displaySize_, int thumbnailSize_)
        {
            double num = ((double)displaySize_) / ((double)originalImageWidth_);
            double num2 = ((double)thumbnailSize_) / ((double)originalImageWidth_);
            this.ImageHeight = Convert.ToInt32((double)(num * originalImageHeight_));
            this.ImageWidth = Convert.ToInt32((double)(num * originalImageWidth_));
            this.ThumbHeight = Convert.ToInt32((double)(num2 * originalImageHeight_));
            this.ThumbWidth = Convert.ToInt32((double)(num2 * originalImageWidth_));
        }
    }

}
