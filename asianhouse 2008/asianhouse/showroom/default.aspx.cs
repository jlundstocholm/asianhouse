﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace asianhouse.showroow
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Text.StringBuilder sbScript = new System.Text.StringBuilder();
            sbScript.Append("<script type='text/javascript'>");
            sbScript.Append(Environment.NewLine);
            sbScript.Append("load();");
            sbScript.Append(Environment.NewLine);
            sbScript.Append("</script>");

            this.ClientScript.RegisterStartupScript(this.GetType(), "onLoadCall", sbScript.ToString());
        }
    }
}
