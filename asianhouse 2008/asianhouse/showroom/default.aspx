﻿<%@ Page Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="asianhouse.showroow._default" %>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAvIPTh2wtFCKKLoM8T2P4LxQz0txhHxdOEPvwCrujU7QIuDcskxRKjyuC-NVtxW-bYu424zb84iHVOw"
      type="text/javascript"></script>
		<script type="text/javascript">

		    //<![CDATA[
    var map;
    var stations;

    function CreateMarker(point, lib, map) {
	    var marker = new GMarker(point);
	    GEvent.addListener(marker, "click", 
	        function() 
	        {
	            map.panTo(marker.getPoint());
                marker.openInfoWindowHtml(lib);
            }
        );
	    return marker;
    }
    
    function load() {
        if (GBrowserIsCompatible()) 
        {
            var map = new GMap2(document.getElementById("map"));
            var center = new GLatLng(56.2009, 9.0136);
            var marker = new GMarker(center, {draggable: true});
            var zoomLevel = 15;
            map.setCenter(center, zoomLevel, G_NORMAL_MAP);
            map.enableDoubleClickZoom();
			map.enableContinuousZoom();
			map.enableScrollWheelZoom();
            new GKeyboardHandler(map);
		    
		    map.addControl(new GSmallMapControl());
		    map.addControl(new GMapTypeControl());
		    map.addControl(new GScaleControl());
		    map.addControl(new GOverviewMapControl(new GSize(200,200)));

		    
		    
            
            GDownloadUrl("locations.xml", 
                function(data, responseCode) 
                {
				    var locationXml = GXml.parse(data);
				    locations = locationXml.documentElement.getElementsByTagName('location');
				    var xmlObj = locationXml.documentElement;
				    for (var i = 0; i < locations.length; i++) 
				    {
				        var uri = '';
				        var duration = '';
				        var rating = '';
				        if (locations[i].hasChildNodes())
				        {
                            info = GXml.value(locations[i].getElementsByTagName("address")[0]);
				        }
					    var point = new GLatLng(parseFloat(locations[i].getAttribute('lat')), parseFloat(locations[i].getAttribute('lng')));
                        map.addOverlay(CreateMarker(point, FormatInfo(info), map));
				    }
                }
            );
            
            function FormatInfo(address)
            {
                var output = '<div class=\"infoText\">';
                output += address + '</div>';
                var ou = output.replace(/\n/g, "<br/>");
                return ou;
            }
            
            GEvent.addListener(marker, "dragend", 
                function() 
                {
                    map.panTo(marker.getPoint());
                }
            );
            
            GEvent.addListener(map, "click", 
                function(marker1, point) 
                {
                    if (marker1)
                    {
                        map.panTo(marker1.getPoint());
                    }
                    else
                    {
                        map.panTo(point);
                        marker.setPoint(point);
                    }
                }
            );
            

        }
    }

    //]]>
    </script>
    
    <br/>
						<h1>Showroom</h1>						
						<div id="map" class="center" style="width:600px;height:500px;margin-left:auto;margin-right:auto;">
						</div>
						<br/>
</asp:Content>
