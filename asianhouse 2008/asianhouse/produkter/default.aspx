﻿<%@ Page Language="C#" Title="Asian House &#174; - Varegrupper" MasterPageFile="~/site.master"
    AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="asianhouse.produkter" %>

<%@ Register Assembly="AsianHouse.Controls" Namespace="AsianHouse.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContent">
    <h1>
        Varegrupper</h1>
        <table>
            <tr>
                <td style="vertical-align:top;">
                
<!-- Venstre kolonne start -->
                <cc1:ItemGroup ItemGroupID="5"  Alignment="Left" ID="ItemGroup1" runat="server" />
                <cc1:ItemGroup ItemGroupID="13" Alignment="Left" ID="ItemGroup2" runat="server" />
                <cc1:ItemGroup ItemGroupID="52" Alignment="Left" ID="ItemGroup45" runat="server" />
                <cc1:ItemGroup ItemGroupID="51" Alignment="Left" ID="ItemGroup3" runat="server" />
                <cc1:ItemGroup ItemGroupID="7"  Alignment="Left" ID="ItemGroup4" runat="server" />
                <cc1:ItemGroup ItemGroupID="8"  Alignment="Left" ID="ItemGroup5" runat="server" />
                <cc1:ItemGroup ItemGroupID="19" Alignment="Left" ID="ItemGroup6" runat="server" />
                <cc1:ItemGroup ItemGroupID="30" Alignment="Left" ID="ItemGroup7" runat="server" />
                <cc1:ItemGroup ItemGroupID="35" Alignment="Left" ID="ItemGroup8" runat="server" />
                <cc1:ItemGroup ItemGroupID="36" Alignment="Left" ID="ItemGroup9" runat="server" />
                <cc1:ItemGroup ItemGroupID="37" Alignment="Left" ID="ItemGroup10" runat="server" />
                <cc1:ItemGroup ItemGroupID="11" Alignment="Left" ID="ItemGroup11" runat="server" />
                <cc1:ItemGroup ItemGroupID="29" Alignment="Left" ID="ItemGroup12" runat="server" />
                <cc1:ItemGroup ItemGroupID="25" Alignment="Left" ID="ItemGroup13" runat="server" />
                <cc1:ItemGroup ItemGroupID="6"  Alignment="Left" ID="ItemGroup14" runat="server" />
                <cc1:ItemGroup ItemGroupID="27" Alignment="Left" ID="ItemGroup15" runat="server" />
                <cc1:ItemGroup ItemGroupID="38" Alignment="Left" ID="ItemGroup16" runat="server" />
                <cc1:ItemGroup ItemGroupID="39" Alignment="Left" ID="ItemGroup17" runat="server" />
                <cc1:ItemGroup ItemGroupID="40" Alignment="Left" ID="ItemGroup18" runat="server" />
                <cc1:ItemGroup ItemGroupID="10" Alignment="Left" ID="ItemGroup19" runat="server" />
                <cc1:ItemGroup ItemGroupID="12" Alignment="Left" ID="ItemGroup20" runat="server" />
                <cc1:ItemGroup ItemGroupID="41" Alignment="Left" ID="ItemGroup21" runat="server" />
                <cc1:ItemGroup ItemGroupID="42" Alignment="Left" ID="ItemGroup22" runat="server" />
                <cc1:ItemGroup ItemGroupID="55" Alignment="Left" ID="ItemGroup55" runat="server" />
<!-- Venstre kolonne slut -->                    
                
                </td>
                <td style="vertical-align:top;">
                
<!-- Højre kolonne start -->
                <cc1:ItemGroup ItemGroupID="14"  Alignment="Right" ID="ItemGroup23" runat="server" />
                <cc1:ItemGroup ItemGroupID="31"  Alignment="Right" ID="ItemGroup24" runat="server" />
                <cc1:ItemGroup ItemGroupID="22"  Alignment="Right" ID="ItemGroup25" runat="server" />
                <cc1:ItemGroup ItemGroupID="16"  Alignment="Right" ID="ItemGroup26" runat="server" />
                <cc1:ItemGroup ItemGroupID="21"  Alignment="Right" ID="ItemGroup27" runat="server" />
                <cc1:ItemGroup ItemGroupID="53"  Alignment="Right" ID="ItemGroup46" runat="server" />
                <cc1:ItemGroup ItemGroupID="1"  Alignment="Right" ID="ItemGroup28" runat="server" />
                <cc1:ItemGroup ItemGroupID="15"  Alignment="Right" ID="ItemGroup29" runat="server" />
                <cc1:ItemGroup ItemGroupID="2"  Alignment="Right" ID="ItemGroup30" runat="server" />
                <cc1:ItemGroup ItemGroupID="43"  Alignment="Right" ID="ItemGroup31" runat="server" />
                <cc1:ItemGroup ItemGroupID="28"  Alignment="Right" ID="ItemGroup32" runat="server" />
                <cc1:ItemGroup ItemGroupID="23"  Alignment="Right" ID="ItemGroup33" runat="server" />
                <cc1:ItemGroup ItemGroupID="3"  Alignment="Right" ID="ItemGroup34" runat="server" />
                <cc1:ItemGroup ItemGroupID="26"  Alignment="Right" ID="ItemGroup35" runat="server" />
                <cc1:ItemGroup ItemGroupID="24"  Alignment="Right" ID="ItemGroup36" runat="server" />
                <cc1:ItemGroup ItemGroupID="44"  Alignment="Right" ID="ItemGroup37" runat="server" />
                <cc1:ItemGroup ItemGroupID="45"  Alignment="Right" ID="ItemGroup38" runat="server" />
                <cc1:ItemGroup ItemGroupID="46"  Alignment="Right" ID="ItemGroup39" runat="server" />
                <cc1:ItemGroup ItemGroupID="47"  Alignment="Right" ID="ItemGroup40" runat="server" />
                <cc1:ItemGroup ItemGroupID="48"  Alignment="Right" ID="ItemGroup41" runat="server" />
                <cc1:ItemGroup ItemGroupID="32"  Alignment="Right" ID="ItemGroup42" runat="server" />
                <cc1:ItemGroup ItemGroupID="49"  Alignment="Right" ID="ItemGroup43" runat="server" />
                <cc1:ItemGroup ItemGroupID="50"  Alignment="Right" ID="ItemGroup44" runat="server" />
                <cc1:ItemGroup ItemGroupID="54"  Alignment="Right" ID="ItemGroup47" runat="server" />
<!-- Højre kolonne slut -->

                </td>
            </tr>
        </table>
</asp:Content>
