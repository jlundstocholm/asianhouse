﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AsianHouse.Web.produkter
{
    public partial class item : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["itemid"]))
            {
                iItem.Src += Request.QueryString["itemid"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["size"]))
                {
                    iItem.Src += "&size=" + Request.QueryString["size"].ToString();
                }
            }
        }
    }
}
