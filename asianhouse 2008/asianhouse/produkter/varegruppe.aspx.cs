﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AsianHouse.Web.produkter
{
    public partial class varegruppe : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                int itemGroupID = -1;
                Int32.TryParse(Request.QueryString["id"].ToString(), out itemGroupID);
                ItemGroupContent1.ItemGroupID = itemGroupID;
            }
            
        }
    }
}
