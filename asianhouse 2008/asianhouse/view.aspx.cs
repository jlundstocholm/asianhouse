using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;

using AsianHouse.IO;

namespace AsianHouse.Web.UI.Forms
{
	/// <summary>
	/// Summary description for Show.
	/// </summary>
	public partial class ViewImage : System.Web.UI.Page
	{
	
		public void Page_Load(object sender, System.EventArgs e)
		{
			
			try 
			{
                if (!string.IsNullOrEmpty(Request.QueryString["picture"]))
                {
                    string folder = "App_Data/billeder/";
                    string fileName = Request.QueryString["picture"];
                    FileInfo fi = new FileInfo(HttpContext.Current.Server.MapPath(folder) + fileName);

                    if (fi.Exists && fi.Directory.FullName.ToUpperInvariant().Contains(Path.DirectorySeparatorChar + "BILLEDER"))
                    {
                        FileStream stream = new FileStream(fi.FullName, FileMode.Open, FileAccess.Read, FileShare.Read);

                        Response.ContentType = "image/jpeg";
                        //SetContentDisposition(file);

                        int bufSize = (int)stream.Length;
                        byte[] buf = new byte[bufSize];

                        int bytesRead = stream.Read(buf, 0, bufSize);
                        stream.Flush();
                        stream.Close();

                        Response.OutputStream.Write(buf, 0, bytesRead);
                    }
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["item"]))
                {
                    string folder = "App_Data/billeder/varenumre/";
                    
                    string fileName = Request.QueryString["item"];

                    AsianHouse.IO.File file = new AsianHouse.IO.File(fileName, Request.QueryString["mode"], Request.QueryString["size"], true);
                    FileInfo fi = new FileInfo(HttpContext.Current.Server.MapPath(folder) + file.FileName);

                    if (fi.Exists && fi.Directory.FullName.ToUpperInvariant().Contains(Path.DirectorySeparatorChar + "BILLEDER\\VARENUMRE"))
                    {
                        FileStream stream = new FileStream(fi.FullName, FileMode.Open, FileAccess.Read, FileShare.Read);

                        Response.ContentType = "image/jpeg";
                        //SetContentDisposition(file);

                        int bufSize = (int)stream.Length;
                        byte[] buf = new byte[bufSize];

                        int bytesRead = stream.Read(buf, 0, bufSize);
                        stream.Flush();
                        stream.Close();

                        Response.OutputStream.Write(buf, 0, bytesRead);
                    }
                }
			}
			catch
			{
				throw;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
