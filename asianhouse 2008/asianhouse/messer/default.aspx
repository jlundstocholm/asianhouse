﻿<%@ Page Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="asianhouse.messer._default" %>
<asp:Content runat="server" ContentPlaceHolderID="MainContent" >
<h1>Messer</h1>
<p style="text-align:center;">
<img src="../view.aspx?picture=messer/messer.jpg" alt="" width="490"/>
</p>
	<asp:Repeater id="rExhibitions" runat="server">
		<HeaderTemplate>
	<table style="Width: 100%;" >
		</HeaderTemplate>
		<ItemTemPlate>
		<tr style="border-top: 2px solid #f00;border-bottom: 2px solid #f00;">
			<td style="vertical-align:top;"><img alt="" src="../view.aspx?picture=messer/<%# DataBinder.Eval(Container.DataItem, "ImageFile").ToString().Trim() %>"/></td>
			<td class="top">
				<h2 style="text-align:left;"><%# DataBinder.Eval(Container.DataItem, "Title").ToString() %></h2>
				<h3 style="text-align:left;"><%# DataBinder.Eval(Container.DataItem, "SubTitle").ToString().Trim() %></h3>
				<h3 style="text-align:left;"><%# DataBinder.Eval(Container.DataItem, "Location").ToString() %></h3>
				<div  style="text-align:left;">
					Fra <%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "StartDate")).ToString("dd.MMM yyyy") %> til <%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "EndDate")).ToString("dd.MMM yyyy") %><br/>
					Hal <%# DataBinder.Eval(Container.DataItem, "Hall").ToString() %>, Stand <%# DataBinder.Eval(Container.DataItem, "Spot").ToString() %>
				</div>
				<p style="text-align:left;">
				    Placering på messe:<br />
				    <br />
				    <a href="../view.aspx?picture=messer/<% = DateTime.Now.Year +1 %>/<%# DataBinder.Eval(Container.DataItem, "Title").ToString() %>_stor.jpg"><img src="../view.aspx?picture=messer/<%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "EndDate")).Year %>/<%# DataBinder.Eval(Container.DataItem, "Title").ToString() %>_lille.jpg" alt="Placering på messe - klik for større billede" height="155px" width="239px" /></a>
				</p>
				<p style="color:#f00;">
				  Ved den store messerokade ved sidste efterårs messe var vi desværre blevet forflyttet til en uheldig placering i Hal K men til den kommende messe har vi fået vores gamle standplacering
                    i Hal L tilbage, så vi er nu at finde samme sted som tidligere.<br />
                    På gensyn i Hal L – lige ved indgang Syd2
                </p>
                <p>
                    Hilsen Helle, Klaus og Eddie
				</p>
			</td>
		</tr>
		</ItemTemplate>
		<FooterTemplate>
	</table>
	</FooterTemplate>
	</asp:Repeater>
</asp:Content>

