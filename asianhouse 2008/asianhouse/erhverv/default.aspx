﻿<%@ Page Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="asianhouse.erhverv._default" %>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">
<br/>
						<h1>Information</h1>
						<div class="center" style="width:500px;margin-left:auto;margin-right:auto;">
							<h2>Erhverv</h2>
							<table width="100%" >
								<tr>
									<td class="top" style="padding-right:10px;width:250px;">
										<h3>Kontakt Asian House for produktoplysninger og priser.</h3>
										<h3>For forhandling af vore produkter, kontakt venligst Eddie Lund Andersen på tlf. 97141027 eller 40421027.</h3>
										<h3>De kan også benytte vores e-mail.</h3>
									</td>
									<td class="top" style="padding-left:10px;width:250px;">
										<h3 style="border-bottom: 1px solid #f00;">Asian House Engros</h3>
										<p>
										Genvejen 20<br/>
										DK-7451 Sunds<br/>
										Danmark<br/>
										(8 km nord for Herning)
										</p>
										<p class="fixedWidth">
											Tlf.: +45 97 14 10 27<br/>
											Bil : +45 40 42 10 27<br/>
											Fax : +45 97 14 11 33
										</p>										
									</td>
								</tr>
							</table>
						</div>
						<br/>
</asp:Content>