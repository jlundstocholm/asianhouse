﻿<%@ Page 
    Language="C#" 
    AutoEventWireup="true" 
    MasterPageFile="~/site.master" 
    CodeBehind="default.aspx.cs" 
    Inherits="AsianHouse.Web.oversigt._default" %>

<asp:Content ContentPlaceHolderID="MainContent" ID="login" runat="server">
    <asp:Login ID="Login1" runat="server" onauthenticate="Login1_Authenticate" 
        DisplayRememberMe="False" 
        FailureText="Ugyldig kombination af brugernavn og password" 
        LoginButtonText="Log Ind" UserNameLabelText="Brugernavn: " 
        UserNameRequiredErrorMessage="Brugernavn er obligatorisk">
    </asp:Login>
</asp:Content>