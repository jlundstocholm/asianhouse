﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AsianHouse.Common.Objects;
using AsianHouse.Common.Interfaces;
using AsianHouse.DataLogic;

namespace AsianHouse.Web.oversigt
{
    public partial class redigervare : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.User.Identity.IsAuthenticated || !this.User.IsInRole("admin"))
                Response.Redirect("/oversigt");

            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["mode"]) && !string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    // delete mode
                    Guid guid = new Guid(Request.QueryString["id"].ToString());
                    Data.DeleteItem(guid);

                    Response.Redirect("soegvare.aspx");

                }
                else
                {

                    if (!Page.IsPostBack)
                    {
                        List<ItemGroup> itemGroups = Data.GetItemGroups();

                        itemGroups.Sort();

                        liItemGroup.DataSource = itemGroups;
                        liItemGroup.DataTextField = "GroupName";
                        liItemGroup.DataValueField = "Id";
                        liItemGroup.DataBind();

                        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                        {
                            IItem item = Data.GetItem(new Guid(Request.QueryString["id"].ToString()));

                            iItemNumber.Value = item.SKU;
                            iName_dk.Value = item.Name;
                            iPrice.Value = item.Price.ToString();
                            iHeight.Value = item.Height.ToString();
                            iWeight.Value = item.Weight.ToString();
                            iLength.Value = item.Length.ToString();
                            iWidth.Value = item.Width.ToString();
                            iDescription.Value = item.Description;
                            iImageFile.Value = item.ImageFile;
                            liItemGroup.SelectedValue = item.ItemGroupID.ToString();

                            imgItem.Src = "../view.aspx?item=" + Request.QueryString["id"].ToString();
                        }
                    }
                }
            }
            catch
            {
            }
        }

        protected void cmdSend_Click(object sender, EventArgs e)
        {

            System.Uri referer = Request.UrlReferrer;

            Item updatedItem = new Item
            {
                Description = iDescription.Value,
                Guid = new Guid(Request.QueryString["id"].ToString()),
                ItemGroupID = Convert.ToInt32(liItemGroup.SelectedValue),
                Name = iName_dk.Value,
                SKU = iItemNumber.Value,
                ImageFile = iImageFile.Value
            };

            updatedItem.Height = iHeight.Value == string.Empty ? null : (double?) Convert.ToDouble(iHeight.Value);
            updatedItem.Length = iLength.Value == string.Empty ? null : (double?)Convert.ToDouble(iLength.Value);
            updatedItem.Price = iPrice.Value == string.Empty ? null : (double?)Convert.ToDouble(iPrice.Value);
            updatedItem.Weight = iWeight.Value == string.Empty ? null : (double?)Convert.ToDouble(iWeight.Value);
            updatedItem.Width = iWidth.Value == string.Empty ? null : (double?)Convert.ToDouble(iWidth.Value);

            if (chkOverskriv.Checked)
            {
                // get image from stream
                if (FileUpload1.PostedFile != null && FileUpload1.PostedFile.ContentLength > 0)
                {
                    string folder = HttpContext.Current.Server.MapPath("~/App_Data/billeder/varenumre/");
                    // delete existing pictures
                    AsianHouse.IO.File.DeletePictureFiles(System.IO.Path.Combine(folder, updatedItem.ImageFile));
                    // create new pictures
                    AsianHouse.IO.File.CreatePictures(FileUpload1.PostedFile.InputStream, FileUpload1.FileName, updatedItem.Guid, folder);
                    updatedItem.ImageFile = String.Format("{0}-{1}", FileUpload1.FileName, updatedItem.Guid);
                }
            }

            Data.UpdateItem(updatedItem);
            Response.Redirect(referer.ToString());
        }
    }
}