﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AsianHouse.Common.Objects;
using AsianHouse.Common.Interfaces;
using AsianHouse.DataLogic;

namespace AsianHouse.Web.oversigt
{
    public partial class tilfoejvare : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.User.Identity.IsAuthenticated || !this.User.IsInRole("admin"))
                Response.Redirect("/oversigt");

            lblStatus.Text = "";


            if (!Page.IsPostBack)
            {
                List<ItemGroup> itemGroups = Data.GetItemGroups();

                itemGroups.Sort();

                liItemGroup.DataSource = itemGroups;
                liItemGroup.DataTextField = "GroupName";
                liItemGroup.DataValueField = "Id";
                liItemGroup.DataBind();

            }
        }

        protected void cmdSend_Click(object sender, EventArgs e)
        {
            Item newItem = new Item
            {
                Description = iDescription.Value,
                ItemGroupID = Convert.ToInt32(liItemGroup.SelectedValue),
                Name = iName_dk.Value,
                SKU = iItemNumber.Value,
            };

            newItem.Height = iHeight.Value == string.Empty ? null : (double?)Convert.ToDouble(iHeight.Value);
            newItem.Length = iLength.Value == string.Empty ? null : (double?)Convert.ToDouble(iLength.Value);
            newItem.Price = iPrice.Value == string.Empty ? null : (double?)Convert.ToDouble(iPrice.Value);
            newItem.Weight = iWeight.Value == string.Empty ? null : (double?)Convert.ToDouble(iWeight.Value);
            newItem.Width = iWidth.Value == string.Empty ? null : (double?)Convert.ToDouble(iWidth.Value);

            newItem.Guid = System.Guid.NewGuid();

            if (FileUpload1.PostedFile != null && FileUpload1.PostedFile.ContentLength > 0)
            {
                string folder = HttpContext.Current.Server.MapPath("~/App_Data/billeder/varenumre/");
                // create new pictures
                AsianHouse.IO.File.CreatePictures(FileUpload1.PostedFile.InputStream, FileUpload1.FileName, newItem.Guid, folder);
                newItem.ImageFile = String.Format("{0}-{1}", FileUpload1.FileName, newItem.Guid);
            }

            Data.CreateItem(newItem);
            lblStatus.Text = "Varen er nu oprettet i systemet";
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            iItemNumber.Value = string.Empty ;
            iName_dk.Value = string.Empty;
            iPrice.Value = string.Empty;
            iHeight.Value = string.Empty;
            iWeight.Value = string.Empty;
            iLength.Value = string.Empty;
            iWidth.Value = string.Empty;
            iDescription.Value = string.Empty;
            liItemGroup.SelectedIndex = 0;
        }
    }
}
