﻿<%@ Page 
    Language="C#" 
    MasterPageFile="~/site.master" 
    AutoEventWireup="true" 
    CodeBehind="tilfoejvare.aspx.cs" 
    Inherits="AsianHouse.Web.oversigt.tilfoejvare" 
%>
<asp:Content ContentPlaceHolderID="MainContent" ID="tilfoejvare" runat="server">
<h1>Tilføj ny vare</h1>
    <table>

		<tr>
			<td>Varenummer</td><td><input id="iItemNumber" type="text" runat="server" size="6" name="iItemNumber"/></td><td rowspan="7" style="text-align: right;"></td>
		</tr>
		<tr>
			<td>Varenavn</td><td><input id="iName_dk" type="text" runat="server" size="40" /></td>
		</tr>
		<tr>
			<td>Pris</td><td><input id="iPrice" type="text" runat="server" size="4" name="iPrice"/>&nbsp;(i hele kroner)</td>
		</tr>
		<tr>
			<td>Højde</td><td><input id="iHeight" type="text" runat="server" size="4" name="iHeight"/>&nbsp;(i hele centimeter)</td>
		</tr>
		<tr>
			<td>Vægt</td><td><input id="iWeight" type="text" runat="server" size="4" name="iWeight"/></td>
		</tr>
		<tr>
			<td>Længde</td><td><input id="iLength" type="text" runat="server" size="4" name="iLength"/>&nbsp;(i hele centimeter)</td>
		</tr>
		<tr>
			<td>Bredde</td><td><input id="iWidth" type="text" runat="server" size="4" name="iWidth"/>&nbsp;(i hele centimeter)</td>
		</tr>
		<tr>
			<td style="vertical-align:top;">Beskrivelse</td><td colspan="2"><textarea runat="server" rows="10" cols="65" id="iDescription" name="iDescription"></textarea></td>
		</tr>									
		<tr>
			<td>Varegruppe</td><td><asp:listbox id="liItemGroup" runat="server" Rows="1"/></td><td></td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td style="vertical-align:top;">Vælg fil</td><td>
            <asp:FileUpload ID="FileUpload1" runat="server" />
		</tr>
		<tr>
			<td colspan="3"><asp:button id="cmdSend" runat="server" Text="Gem" 
                    onclick="cmdSend_Click"/>&nbsp;<asp:button id="btnReset" runat="server" 
                    Text="Nulstil formular" onclick="btnReset_Click" 
                    /></td>
		</tr>
		<tr>
		    <td colspan="3"><asp:Label ID="lblStatus" runat="server" /></td>
		</tr>
	</table>	

</asp:Content>