﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AsianHouse.Common;
using AsianHouse.DataLogic;

using AsianHouse.Common.Interfaces;

namespace AsianHouse.Web.oversigt
{
    public partial class soegvare : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.User.Identity.IsAuthenticated || !this.User.IsInRole("admin"))
                Response.Redirect("/oversigt");


            Page.Form.DefaultFocus = "txtSearchforItems";
            //Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + txtSearchforItems.ClientID + "').focus();</script>");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            List<IItem> items = Data.SearchForItems(txtSearchforItems.Text);

            rItemGroupContent.DataSource = items;
            rItemGroupContent.DataBind();
        }
    }
}
