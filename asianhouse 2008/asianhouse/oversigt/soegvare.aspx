﻿<%@ Page Language="C#" 
    Title="Asian House &#174; - " 
    MasterPageFile="~/site.master"
    AutoEventWireup="true" 
    CodeBehind="soegvare.aspx.cs" 
    Inherits="AsianHouse.Web.oversigt.soegvare" 
%>

<asp:Content ContentPlaceHolderID="MainContent" ID="adminvaresoeg" runat="server">
    <h1>Søg efter varer</h1>
    <div style="text-align:center;">
        <asp:TextBox ID="txtSearchforItems" runat="server" Width="319px"></asp:TextBox>&nbsp;<asp:Button 
            ID="Button1" runat="server" Text="Søg" onclick="Button1_Click" 
            Width="62px" />
        <br />
        <br />
    </div>
    <asp:Repeater id="rItemGroupContent" runat="server">
	<HeaderTemplate>
		<div style="width:600px;margin-left:auto;margin-right:auto;">
		<table width="100%">
	</HeaderTemplate>
	<ItemTemplate>
		<tr style="background-color: #000000;">
			<td><a href="redigervare.aspx?id=<%# DataBinder.Eval(Container.DataItem, "Guid").ToString() %>">Rediger</a> / <a onclick="return confirm('Er du sikker på, at du vil slette denne vare?');" href="redigervare.aspx?mode=delete&id=<%# DataBinder.Eval(Container.DataItem, "Guid").ToString() %>">slet</a></td>
			<td>Produkt</td>
			<td>Varenummer</td>
		</tr>
		<tr style="vertical-align:top;">
			<td rowspan="2">
				<img style="border: 1px solid #f00;" src="../view.aspx?item=<%# DataBinder.Eval(Container.DataItem, "Guid").ToString().Trim() %>" alt="<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>"/>
			</td>
			<td ><%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></td>
			<td class="center"><%# DataBinder.Eval(Container.DataItem, "SKU").ToString() %></td>
		</tr>
		<tr style="vertical-align:top;">
			<td colspan="7" style="text-align:left;"><%# DataBinder.Eval(Container.DataItem, "Description").ToString() %></td>
		</tr>
	</ItemTemplate>
</asp:Repeater>
</asp:Content>