﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using AsianHouse.DataLogic;

namespace AsianHouse.Web.oversigt
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {

            Login1.FailureText = "Ugyldig kombination af brugernavn og password.";


            var userIdentity = Data.GetUserIdentity(Login1.UserName, Login1.Password);

            e.Authenticated = userIdentity.IsAuthenticated;

            if (!userIdentity.IsAuthenticated)
                return;

            var userData = string.Format("{0}", userIdentity.Roles);
            var ticket = new FormsAuthenticationTicket(1, userIdentity.Name, DateTime.Now, DateTime.Now.AddMinutes(30), true, userData);
            var encTicket = FormsAuthentication.Encrypt(ticket);
            var faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            Response.Cookies.Add(faCookie);
            var redirectUrl = FormsAuthentication.GetRedirectUrl(userIdentity.Name, false);
            Response.Redirect(redirectUrl);
        }
    }
}
