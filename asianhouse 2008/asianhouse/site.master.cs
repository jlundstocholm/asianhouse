﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected Literal LoggedInUser;
    protected Literal Version;
    protected HyperLink linkLogOut;
    protected HyperLink linkLogIn;
    protected HyperLink linkAdmin;

    protected void Page_Load(object sender, EventArgs e)
    {


        if (this.Context.User.Identity.Name != "")
        {
            // User is logged in
            LoggedInUser.Text = string.Format("Logget ind som: {0}", this.Context.User.Identity.Name);
            LoggedInUser.Visible = true;
            linkLogOut.Visible = true;
            linkLogIn.Visible = false;

            if (Context.User.IsInRole("admin"))
                linkAdmin.Visible = true;
        }
        else
        {
            // User is not logged in
            LoggedInUser.Visible = false;
            linkLogOut.Visible = false;
            linkLogIn.Visible = true;
            linkAdmin.Visible = false;
        }

        //Version.Text = GetType().Assembly.GetName().Version.ToString();
    }
}
