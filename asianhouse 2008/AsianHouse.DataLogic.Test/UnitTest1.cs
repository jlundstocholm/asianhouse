﻿
using NUnit.Framework;

namespace AsianHouse.DataLogic.Test
{
    [TestFixture]
    public class TestOfData
    {
        [Test]
        public void TestCreateUser()
        {
            var result = Data.CreateUser("jesper", "pswd", "admin", "email");

            Assert.AreEqual(1, result);
        }
    }
}
