﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AsianHouse.Startup))]
namespace AsianHouse
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
