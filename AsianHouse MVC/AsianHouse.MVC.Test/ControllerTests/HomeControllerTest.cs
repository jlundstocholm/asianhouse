﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AsianHouse.MVC.Controllers;
using NUnit.Framework;

namespace AsianHouse.MVC.Test.ControllerTests
{
    [TestFixture]
    public class HomeControllerTest
    {
        [Test]
        public void Default_Action_Returns_Index_View()
        {
            //Arrange
            const string viewName = "Index";
            var homeController = new HomeController();

            // Act
            var result = homeController.Index();

            // Assert
            Assert.IsNotNull(result, "Should have returned a ViewResult");
            Assert.AreEqual("Forside", (result as ViewResult).ViewBag.Title, "View name should have been {0}", viewName);
        }

        public static class AssertActionResult
        {
            public static void IsContentResult(ActionResult result, string contentToMatch)
            {
                var contentResult = result as ContentResult;
                Assert.NotNull(contentResult);
                Assert.AreEqual(contentToMatch, contentResult.Content);
            }
        }
    }
}
