﻿using System;
using System.Linq;
using NHibernate;
using NHibernate.Linq;

namespace AsianHouse.Domain.Repositories
{
    public class Repository<T> : IRepository<T>//, IDependency
    {
        private readonly ISession _session;

        public Repository(ISession session)
        {
            _session = session;
        }

        public T FindById(object id)
        {
            return _session.Get<T>(id);
        }

        public IQueryable<T> GetAll()
        {
            return _session.Query<T>();
        }

        public void Save(T obj)
        {
            _session.Save(obj);
        }

        public void Delete(T obj)
        {
            _session.Delete(obj);
        }

        public void Update(T obj)
        {
            _session.Update(obj);
        }

        public IQueryable<T> Filter(Func<T, bool> predicate)
        {
            return _session.Query<T>().Where(predicate).AsQueryable();
        }
    }

    //public class TestRepository : IRepository, IDependency
    //{
    //    #region Implementation of IRepository

    //    public long GetID()
    //    {
    //        return DateTime.Now.Ticks;
    //    }

    //    #endregion
    //}
}