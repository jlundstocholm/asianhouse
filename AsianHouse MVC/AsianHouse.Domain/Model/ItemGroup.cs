﻿using System;
using System.Collections.Generic;

namespace AsianHouse.Domain.Model
{
    public class ItemGroup
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual Guid Guid { get; set; }
        public virtual int Priority { get; set; }
        public virtual string ImageFile { get; set; }
        public virtual IList<Item> Items { get; set; }

        public ItemGroup()
        {
            Items = new List<Item>();
        }
    }
}