﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AsianHouse.Domain.Model
{
        //UserID           int(10) unsigned PK        //UserName         varchar(45)
        //Roles            varchar(45)
        //Password         varchar(100)
        //CreationDate     timestamp
        //EmailAddress     varchar(100)

    public class User
    {
        public User()
        {
            //Roles = new List<Role>();
        }
        public virtual int Id { get; set; }
        public virtual string UserName { get; set; }
        public virtual string Password { get; set; }
        public virtual string Email { get; set; }
        //public virtual IList<Role> Roles { get; set; }
    }
}
