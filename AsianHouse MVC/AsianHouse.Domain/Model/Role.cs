﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AsianHouse.Domain.Model
{
    //RoleID           int(10) unsigned PK    //Role             varchar(45)

    public class Role
    {
        public virtual int Id { get; set; }
        public virtual string RoleName { get; set; }
        //public vir    tual IList<User> Users { get; set; }
        public virtual int UserId { get; set; }
    }
}
