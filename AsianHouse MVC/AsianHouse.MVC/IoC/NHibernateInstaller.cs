﻿using AsianHouse.MVC.NHibernate;
using Castle.Facilities.FactorySupport;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using NHibernate;

namespace AsianHouse.MVC
{
    public class NHibernateInstaller : IWindsorInstaller
    {
        #region Implementation of IWindsorInstaller

        public void Install(IWindsorContainer container, IConfigurationStore store) 
        {
            container.AddFacility<FactorySupportFacility>();

            container.Register(Component.For<ISessionFactory>()
                                   .LifeStyle.Singleton
                                   .UsingFactoryMethod(NHibernateHelper.CreateSessionFactory));

            container.Register(Component.For<ISession>()
                                   .LifeStyle.PerWebRequest
                                   .UsingFactoryMethod(kernel => kernel.Resolve<ISessionFactory>().OpenSession()));
        }

        #endregion
    }
}