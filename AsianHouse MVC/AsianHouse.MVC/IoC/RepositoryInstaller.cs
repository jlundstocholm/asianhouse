﻿using System.Reflection;
using System.Web.Security;
using AsianHouse.Domain.Repositories;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace AsianHouse.MVC
{
    public class RepositoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(AllTypes
                                   .FromAssembly(Assembly.Load("AsianHouse.Domain"))
                                   .BasedOn(typeof(IRepository<>))
                                   .WithService.FirstInterface()
                                   .Configure(component => component.LifeStyle.Transient));
        }
    }
}