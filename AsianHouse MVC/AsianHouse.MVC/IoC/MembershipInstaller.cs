﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using AsianHouse.Security;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace AsianHouse.MVC
{
    public class MembershipInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<AHMembershipProvider>()
                .LifeStyle.Transient
                .Named("AsianHouseCustomMembershipProvider"));
        }
    }
}