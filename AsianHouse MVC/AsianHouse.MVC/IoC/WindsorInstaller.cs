﻿//using System;
//using System.Web;
//using System.Web.Mvc;
//using AsianHouse.MVC.IoC;
//using AsianHouse.MVC.Models;
//using AsianHouse.MVC.Models.Repositories;
//using Castle.Facilities.FactorySupport;
//using Castle.MicroKernel.Registration;
//using Castle.MicroKernel.SubSystems.Configuration;
//using Castle.Windsor;

//namespace AsianHouse.MVC
//{
//    public class WindsorInstaller : IWindsorInstaller
//    {
//        public void Install(IWindsorContainer container, IConfigurationStore store)
//        {
//            // Register all controllers from this assembly
//            container.Register(
//                AllTypes.FromThisAssembly()
//                .BasedOn<Controller>()
//                .Configure(c => c.LifeStyle.PerWebRequest)
//            );


//            container.Register(AllTypes
//                .FromThisAssembly()
//                .BasedOn<IDependency>()
//                .WithService.FirstInterface()
//                .Configure(component => component.LifeStyle.Transient));

//            // Respository and Service registrations
//            container.Register(Component.For<IRepository<ItemGroup>>().ImplementedBy<Repository<ItemGroup>>());
//            //container.Register(Component.For<IRepository>().ImplementedBy<TestRepository>());
//            //container.Register(Component.For<ISessionValidator>().ImplementedBy<SessionValidator>().LifeStyle.PerWebRequest);
//        }
//    }
//}