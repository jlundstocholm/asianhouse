﻿using System.Linq;
using System.Web.Mvc;
using AsianHouse.Domain.Model;
using AsianHouse.Domain.Repositories;

namespace AsianHouse.MVC.Controllers
{
    public class ItemGroupController : Controller
    {
        private readonly IRepository<ItemGroup> _repository;

        public ItemGroupController(IRepository<ItemGroup> repository)
        {
            this._repository = repository;
        }

        // GET: /ItemGroup/
        public ActionResult Index()
        {
            ViewBag.Message = "Liste over varegrupper";

            var itemGroupList = _repository.GetAll();

            //var session = NHibernateHelper.CreateSessionFactory().OpenSession();

            //IQueryable<ItemGroup> rep = session.Query<ItemGroup>();

            //var itemg = rep.Single(ig => ig.Id == 2);


            //var iSession = NHibernateHelper.CreateSessionFactory();
            //var repo = new Repository<ItemGroup>(iSession.OpenSession());
            //var result = repo.FindById(1);

            return View("Index", itemGroupList.ToList());
        }

        //
        // GET: /ItemGroup/Details/5
        public ActionResult Details(int id)
        {

            var itemGroup = _repository.FindById(id);

            ViewBag.Title = itemGroup.Name;

            return View(itemGroup);

        }

        //
        // GET: /ItemGroup/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ItemGroup/Create

        [HttpPost, Authorize(Roles = "admin")]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /ItemGroup/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /ItemGroup/Edit/5

        [HttpPost, Authorize(Roles = "admin")]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /ItemGroup/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /ItemGroup/Delete/5

        [HttpPost, Authorize(Roles = "admin")]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
    