﻿using System.Web.Mvc;

namespace AsianHouse.MVC.Controllers
{
    public class HomeController : Controller
    {
        //private readonly IRepository _repository;

        //public HomeController(IRepository repository)
        //{
        //    _repository = repository;
        //}

        public ActionResult Index()
        {
            ViewBag.Title = "Forside";
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
