﻿function PreLoadImages() {
    var velkommen = new Image();
    var info = new Image();
    var nyheder = new Image();
    var produkter = new Image();
    var privat = new Image();
    var forhandler = new Image();
    var messer = new Image();
    var showroom = new Image();
    var email = new Image();

    velkommen.src = '/billeder/velkommen.gif';
    info.src = '/billeder/info_gold.gif';
    nyheder.src = '/billeder/nyheder_gold.gif';
    produkter.src = '/billeder/produkter_gold.gif';
    privat.src = '/billeder/privat_gold.gif';
    forhandler.src = '/billeder/forhandler_gold.gif';
    messer.src = '/billeder/messer_gold.gif';
    showroom.src = '/billeder/showroom_gold.gif';
    email.src = '/billeder/email_gold.gif';
}


function pullCurtains(distance) {
    var currentLeft; var currentRight; var newLeft; var newRight;
    currentLeft = parseInt(document.getElementById('leftCurtain').style.left);
    currentRight = parseInt(document.getElementById('rightCurtain').style.left);

    newLeft = currentLeft - distance;
    newRight = currentRight + distance;

    if (newLeft < 0 && newRight > 660)
        clearInterval(pull);
    else {
        document.getElementById('leftCurtain').style.left = newLeft + 'px';
        document.getElementById('rightCurtain').style.left = newRight + 'px';
    }
}

function PullMe() {
    pull = setInterval("pullCurtains(10)", 1);
}