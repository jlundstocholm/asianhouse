﻿using System;
using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;

namespace AsianHouse.MVC.NHibernate
{
    public static class NHibernateHelper
    {
        public static ISessionFactory CreateSessionFactory()
        {
            try
            {
#if DEBUG
                return Fluently.Configure()
                    .Database(MySQLConfiguration.Standard.ShowSql().ConnectionString(cf => cf.FromConnectionStringWithKey("MySql")))
                    .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly())
                    .ExportTo(@"C:\Windows\Temp\Fluent"))
                    .BuildSessionFactory();
#else
            return Fluently.Configure()
                .Database(MySQLConfiguration.Standard.ShowSql().ConnectionString(cf => cf.FromConnectionStringWithKey("MySql")))
                .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly()))
                .BuildSessionFactory();
#endif                
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}