﻿using AsianHouse.Domain.Model;
using FluentNHibernate.Mapping;

namespace AsianHouse.MVC.NHibernate.Mapping
{
    public class ItemGroupMap : ClassMap<ItemGroup>
    {
        public ItemGroupMap()
        {
            Table("ItemGroups");
            Id(x => x.Id);

            Map(x => x.Guid);
            Map(x => x.ImageFile);
            Map(x => x.IsActive).Column("Active");
            Map(x => x.Name).Column("GroupName");
            Map(x => x.Priority);

            HasMany(x => x.Items)
                .KeyColumn("ItemGroup")
              .Inverse()
              .Cascade.All();
        }
    }
}