﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AsianHouse.Domain.Model;
using FluentNHibernate.Mapping;

namespace AsianHouse.MVC.NHibernate.Mapping
{
    public class RoleMap : ClassMap<Role>
    {
        public RoleMap()
        {
            Table("vwUserInRoles");

            Id(x => x.Id);

            Map(x => x.RoleName);
            Map(x => x.UserId);


            //HasMany(x => x.Users)
            //    .KeyColumn("UserId")
            //  .Inverse()
            //  .Cascade.All();
        }

    }
}