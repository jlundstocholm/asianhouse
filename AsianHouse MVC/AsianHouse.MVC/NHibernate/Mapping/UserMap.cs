﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AsianHouse.Domain.Model;
using FluentNHibernate.Mapping;

namespace AsianHouse.MVC.NHibernate.Mapping
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Table("Users");

            //UserID           int(10) unsigned PK
            //UserName         varchar(45)
            //Roles            varchar(45)
            //Password         varchar(100)
            //CreationDate     timestamp
            //EmailAddress     varchar(100)

            Id(x => x.Id).Column("UserID");

            Map(x => x.UserName);
            //Map(x => x.Roles);
            Map(x => x.Password);
            Map(x => x.Email).Column("EmailAddress");

            //HasMany(x => x.Roles)
            //    .KeyColumn("UserId")
            //  .Inverse()
            //  .Cascade.All();
        }
    }
}