﻿using AsianHouse.Domain.Model;
using FluentNHibernate.Mapping;

namespace AsianHouse.MVC.NHibernate.Mapping
{
    public class ItemMap : ClassMap<Item>
    {
        public ItemMap()
        {
            Table("Items");

            Id(x => x.Id).Column("ItemNumber");

            Map(x => x.Name).Column("ItemName_DK");
            Map(x => x.CreationDate);
            Map(x => x.Description);
            Map(x => x.Guid);
            Map(x => x.Height);
            Map(x => x.ImageFile);
            Map(x => x.IsActive).Column("Active");
            Map(x => x.Length);
            Map(x => x.Price);
            Map(x => x.SortOrder);
            Map(x => x.Width);
            
            References(x => x.ItemGroup)
                .Column("ItemGroup")
                .Cascade.All();
        }
    }
}