﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace AsianHouse.Security
{
    public static class Configuration
    {
        public static int MinPasswordLength { get; set; }
        public static string PasswordSalt { get; set; }
        public static string MembershipProviderName { get; set; }

        static Configuration()
        {
            var membershipSection = (MembershipSection)WebConfigurationManager.GetSection("system.web/membership");
            var defaultProvider = membershipSection.DefaultProvider;
            var providerSettings = membershipSection.Providers[defaultProvider];

            MembershipProviderName = defaultProvider;

            var minPasswordLength = providerSettings.Parameters["minRequiredPasswordLength"];
            MinPasswordLength = Convert.ToInt32(minPasswordLength);




            PasswordSalt = ConfigurationManager.AppSettings["PasswordSalt"];
        }
    }
}